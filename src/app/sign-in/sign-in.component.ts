import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  form:FormGroup;
  constructor(private fb:FormBuilder) { 

    
  }

  ngOnInit() {
    this.buildForm();

  }

buildForm() {
  console.log("build form is working");
  this.form = this.fb.group({
    email: ['', Validators.compose([
  Validators.required,
  ])],
  password: ['', Validators.compose([
  Validators.required,
  ])],
  
  });
  
  }

  myMethode() {
    console.log("my methode is working");
    console.log(this.form.value);
    console.log(this.form.value.password)
  }

}

