import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  form: FormGroup;

  constructor(private fb:FormBuilder,
    ) { 


  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm()
  {
  console.log("build form is working");
  this.form = this.fb.group({
  firstName: ['', Validators.compose([
  Validators.required,
  ])],
  lastName: ['', Validators.compose([
  Validators.required,
  ])],
  email: ['', Validators.compose([
    Validators.required,
    ])],
  password: ['', Validators.compose([
    Validators.required,
    ])],
    confirmpassword: ['', Validators.compose([
      Validators.required,
    ])],
  
  });
  }
  mySignup(){
    console.log("my program is working!");
    console.log(this.form.value);
    console.log(this.form.value.firstName)
    console.log(this.form.value.lastName)
    console.log(this.form.value.email)
    console.log(this.form.value.password)
    console.log(this.form.value.confirmpassword)
  }
}
